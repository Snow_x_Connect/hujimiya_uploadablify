#!/usr/bin/env node
const getUploadPaths = require("./get_upload_paths").getUploadPaths;
const lib = require("../lib");
const args = require("args");
const process = require("process");
const Toolbox = require("../lib/toolbox");
let cwd = process.cwd();
let flags = args.parse(process.argv);
let sub = args.sub;


const WEBHOOK = "https://hook.worktile.com/incoming/b18a32c12dae4c11a438a9c57bbec959"

getUploadPaths(cwd, sub).then(async avai_paths => {
    for (let ap of avai_paths) {
        if (ap.stats.isDirectory()) {
            console.log("start uploadablify..", ap.full_path);
            await lib.uploadablify_kd_dirVerEach(ap.full_path)
        }
    }
    process.exit()
})