必须注意WINRAR的版本 似乎某些旧版本的winrar不支持UTF8
我目前用的是winrar 5.7版本
5.3版本似乎就不支持的！

### RAR在windows上使用utf8的关键

```
-sc<字符集>[对象]
            指定字符集。

            “字符集”参数是强制的, 可以使用下列值:

              U - Unicode UTF-16;
              F - Unicode UTF-8;
              A - 本机单字节编码，这是 ANSI Windows 版本;
              O - OEM (DOS) 编码。仅用于 Windows 版本;

            源 UTF-16 文件（例如列表文件或注释）的端字节序（Endianness），基于
            字节序标记来检测。
            如果丢失字节序标记，假定为小端字节序（little endian）编码。

            “对象”参数是可选的, 可以使用下列值:

              G - -ilog 参数生成的日志文件;
              L - 列表文件;
              C - 命令文件。
              R - 发送至重定向文件和管道的消息（仅 Windows）。 

            它允许指定不只一个对象, 例如, -scolc。如果 '对象' 参数丢失, '字符
            集' 会应用到所有对象。

            此参数允许指定在 -z[文件] 参数中的文件的字符集, 列表文件和注释文
            件在 "cw" 命令中写入。l

            例子:

            1) rar a -scol data @list

            使用 OEM 编码读取 'list' 中包含的名称。

            2) rar c -scuc -zcomment.txt data

            读取 comment.txt 为 Unicode 文件。

            3) rar cw -scuc data comment.txt

            写入 comment.txt 为 Unicode 文件。

            4) rar lb -scur data > list.txt

            以 Unicode 格式将 data.rar 中的压缩文件名称保存到 list.txt。
```

所以,用`-scfcr`可以保证读取注释文件时使用utf8,输出控制台的信息也是utf8的,方便程序调用

### git
 git remote set-url ecoding https://ptlprgtrt5cs:7fad1f9b0d44b195c1b06d511a44de9dcac903ab@e.coding.net/hujimiya_/hujimiya_uploadablify.git